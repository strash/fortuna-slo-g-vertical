shader_type canvas_item;

uniform float SPEED_X: hint_range(-1.0, 1.0) = 0.4;
uniform float SPEED_Y: hint_range(-1.0, 1.0) = 0.4;
uniform bool CHANGE_COLOR = true;

vec4 set_color(inout vec4 c, in float t) {
	c.r = sin(t * SPEED_X);
	c.b = cos(t * SPEED_Y);
	return clamp(c, 0.0, 1.0);
}


void fragment() {
	vec2 uv = UV;
	uv.x += TIME * SPEED_X;
	uv.y += TIME * SPEED_Y;

	vec4 color = texture(TEXTURE, uv);
	if (CHANGE_COLOR) {
		COLOR = set_color(color, TIME);
	} else {
		COLOR = color;
	}
}
