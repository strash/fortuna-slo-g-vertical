extends Control


onready var STACK_CONTAINER: Control = $Stack as Control
export var START_SCREEN: int

# стэк вьюх состоящий из имен вьюх
var VIEW_STACK: PoolIntArray = PoolIntArray([])


# BUILTINS -------------------------


const TWEEN_SPEED: float = 0.3
var _t: bool


func _ready() -> void:
	randomize()
	add_view_to_stack(START_SCREEN, {})

	var _c: int
	# подключение сигналов
	_c = ($GUI as Control).connect("btn_back_pressed", self, "_on_GUI_btn_back_pressed")
	_c = ($GUI as Control).connect("btn_spin_pressed", self, "_on_GUI_btn_spin_pressed")
	_c = ($GUI as Control).connect("btn_autospin_pressed", self, "_on_GUI_btn_autospin_pressed")


# METHODS -------------------------


# добавление вьюхи в стэк и отображение ее в интерфейсе
func add_view_to_stack(view: int, payload: Dictionary) -> void:
	# подгружаем новую вьюху, вставляем в экран
	var next_view: Node
	next_view = (load(Global.get("VIEWS")[view]) as PackedScene).instance()
	# если во вьюхе есть сигналы переключения вьюх, то коннектим их
	var _c: int
	if next_view.has_signal("change_view_requested"):
		_c = next_view.connect("change_view_requested", self, "_on_View_change_view_requested")
	# управляем отображением элементов GUI согласно настройкам вьюхи
	if next_view.get("GUI_SETUP") != null:
		var gui_setup: Dictionary = next_view.get("GUI_SETUP")
		for keys in gui_setup:
			($GUI as Control).set(keys, gui_setup[keys])
	# добавляем в основную вьюху
	STACK_CONTAINER.add_child(next_view)
	# добавляем пейлоад во вьюху
	if next_view.get("payload") != null:
		next_view.set("payload", payload)
	VIEW_STACK.append(next_view.get_instance_id())
	# анимируем открытие вьюхи
	_t = ($Tween as Tween).interpolate_property(next_view, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# удаление вьюхи из стэка и удаление из интерфейса
func remove_view_from_stack() -> void:
	if not VIEW_STACK.empty():
		# дисконнектим текущую вьюху и мягко удаляем
		var view = instance_from_id(VIEW_STACK[VIEW_STACK.size() - 1])
		# управляем отображением элементов GUI согласно настройкам вьюхи
		if not VIEW_STACK.empty() and VIEW_STACK.size() - 1 > 0:
			var prew_view = instance_from_id(VIEW_STACK[VIEW_STACK.size() - 2])
			if prew_view.get("GUI_SETUP") != null:
				var gui_setup: Dictionary = prew_view.get("GUI_SETUP")
				for keys in gui_setup:
					($GUI as Control).set(keys, gui_setup[keys])
		# анимируем закрытие вьюхи
		_t = ($Tween as Tween).interpolate_property(view, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield($Tween, "tween_all_completed")
		# удаляем вьюху
		VIEW_STACK.remove(VIEW_STACK.size() - 1)
		if view.has_signal("change_view_requested"):
			view.disconnect("change_view_requested", self, "_on_View_change_view_requested")
		view.queue_free()


func _is_slot_game_scene_active() -> bool:
	var game_scene
	for i in VIEW_STACK:
		var game = instance_from_id(i)
		if game.name == "SlotGame":
			game_scene = game
	return true if game_scene != null else false


# SETGET -------------------------


# SIGNALS -------------------------


# вызывается, когда вьюха запрашивает открытие другой вьюхи
# то есть была нажата кнопка в этой вьюхе, которая должна открыть другую вьюху
func _on_View_change_view_requested(view: int, payload: Dictionary = {}) -> void:
	add_view_to_stack(view, payload)


# при нажатии кнопки назад в шапке
func _on_GUI_btn_back_pressed() -> void:
	remove_view_from_stack()


# при нажатии на кнопку спина
func _on_GUI_btn_spin_pressed() -> void:
	if _is_slot_game_scene_active():
		get_node("Stack/SlotGame").call_deferred("spin")


# при нажатии на кнопку автоспина
func _on_GUI_btn_autospin_pressed() -> void:
	if _is_slot_game_scene_active():
		get_node("Stack/SlotGame").call_deferred("auto_spin")


