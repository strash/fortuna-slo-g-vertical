extends Control


signal change_view_requested


# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": true,
	"is_footer_block_visible": false,
	"is_score_block_visible": true,
	"is_level_block_visible": true,
	"is_btn_back_block_visible": true,
}

const LEVEL_ICONS: Array = [
		#preload("res://Resources/Sprites/Slot/ic_lvl_1.png"),
		#preload("res://Resources/Sprites/Slot/ic_lvl_2.png"),
		#preload("res://Resources/Sprites/Slot/ic_lvl_3.png"),
		#preload("res://Resources/Sprites/Slot/ic_lvl_4.png"),
		#preload("res://Resources/Sprites/Slot/ic_lvl_5.png"),
]

var payload: Dictionary = {} setget set_payload


# BUILTINS -------------------------


# METHODS -------------------------


func prepare_view(level: int) -> void:
	($BG as TextureRect).texture = Global.BGS[level - 1]
	($LevelIcon as TextureRect).texture = LEVEL_ICONS[level - 1]


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	prepare_view(payload.level)


# SIGNALS -------------------------


func _on_BtnPlay_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT, payload)


func _on_BtnInfo_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").INFO, payload)


