extends Control


const Column: PackedScene = preload("res://Scenes/Components/Column.tscn")


# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": true,
	"is_footer_block_visible": true,
	"is_score_block_visible": true,
	"is_level_block_visible": true,
	"is_btn_back_block_visible": true,
}


enum { UP, DOWN }


var payload: Dictionary = {} setget set_payload
var textures: Array = [] # текстуры иконок всех уровней

var spinning: bool = false setget set_spinning # состояние спина
var auto_spinning: bool = false setget set_auto_spinning # состояние автоспина


# BUILTINS -------------------------


func _ready() -> void:
	# подгрузка текстур иконок
	for i in Global.get("PROPS").size():
		textures.append([])
		for j in Global.get("PROPS")[i].variations:
			var icon: Texture = load("res://Resources/Sprites/Slot/ic_lvl_%s/%s.png" % [i + 1, j + 1])
			textures[i].append(icon)
	get_node("/root/Main/GUI").call_deferred("change_autospin_count")


# METHODS -------------------------


# setget spinning
func set_spinning(status: bool) -> void:
	spinning = status
	get_node("/root/Main/GUI").set("spinning", status)


# setget auto_spinning
func set_auto_spinning(status: bool) -> void:
	auto_spinning = status
	get_node("/root/Main/GUI").set("auto_spinning", status)
	get_node("/root/Main/GUI").call_deferred("show_hide_autospin_progress", status)


# загрузка уровня
func prepare_level(level: int) -> void:
	($Bg as TextureRect).texture = Global.get("BGS")[level - 1]
	($BoardBg as TextureRect).texture = Global.get("BOARDS")[level - 1]
	($BoardBg as TextureRect).set_size(Vector2.ZERO)
	var shadows: Array = Global.get("SHADOWS")
	($ShadowTop as TextureRect).hide()
	($ShadowBottom as TextureRect).hide()
	if not shadows.empty():
		if shadows.size() >= level:
			if not shadows[level - 1].empty():
				($ShadowTop as TextureRect).texture = Global.get("SHADOWS")[level - 1][0]
				($ShadowTop as TextureRect).set_size(Vector2.ZERO)
				($ShadowTop as TextureRect).show()
			if shadows[level - 1].size() == 2:
				($ShadowBottom as TextureRect).texture = Global.get("SHADOWS")[level - 1][1]
				($ShadowBottom as TextureRect).set_size(Vector2.ZERO)
				($ShadowBottom as TextureRect).show()
	var prop: Dictionary = Global.get("PROPS")[level - 1]
	var board_w: float = prop.count_x * prop.w + prop.offset_x * (prop.count_x - 1)
	var board_h: float = prop.count_y * prop.h + prop.offset_y * (prop.count_y - 1)
	var offset: Vector2 = (self.get_rect().size - Vector2(board_w, board_h)) / 2 + Vector2(prop.icons_offset_x, prop.icons_offset_y)
	var offset_bg: Vector2 = (self.get_rect().size - ($BoardBg as TextureRect).get_rect().size) / 2 + Vector2(prop.board_bg_offset_x, prop.board_bg_offset_y)
	($Board as Control).set_size(Vector2(board_w, board_h))
	($Board as Control).set_position(offset)
	($BoardBg as TextureRect).set_position(offset_bg)
	var shadow_top: Vector2 = Vector2(($BoardBg as TextureRect).get_rect().size.x / 2 - ($ShadowTop as TextureRect).get_rect().size.x / 2 + offset_bg.x, offset_bg.y + prop.shadow_top_offset_y)
	var shadow_bottom: Vector2 = Vector2(($BoardBg as TextureRect).get_rect().size.x / 2 - ($ShadowBottom as TextureRect).get_rect().size.x / 2 + offset_bg.x, offset_bg.y + ($BoardBg as TextureRect).get_rect().size.y - ($ShadowBottom as TextureRect).get_rect().size.y + prop.shadow_bottom_offset_y)
	($ShadowTop as TextureRect).set_position(shadow_top)
	($ShadowBottom as TextureRect).set_position(shadow_bottom)
	for i in prop.count_x:
		var column: Control = Column.instance() as Control
		column.set("props", prop)
		column.set("textures", textures[level - 1])

		# чтобы в разные стороны крутилось
		if i % 2 == 1:

		# или
#		if i == 0 or i == prop.count_x - 1:
			column.set("direction", UP)
		else:
			column.set("direction", DOWN)

		# чтобы в одну сторону
		# column.set("direction", DOWN)

		column.set_position(Vector2((prop.w + prop.offset_x) * i, 0.0))
		column.set_size(Vector2(prop.w, board_h))
		($Board as Control).add_child(column)


# кручение слотов
func spin() -> void:
	if not spinning:
		set_spinning(true)
		var j: float = 0.0
		# НАЧАЛО КРУЧЕНИЯ
		for i in ($Board as Control).get_children():
			if not i is TextureRect:
				# задержка через одну колонку
				match j:
					1.0, 3.0:
						i.move_icons(0.2)
					2.0, 0.0, 4.0:
						i.move_icons(0.0)

				# задержка слева направо
				# i.move_icons(j / 7.0)

				j += 1.0
		yield(get_tree().create_timer(Global.get("SPIN_TIME")), "timeout")
		j = 0.0
		# ОСТАНОВКА КРУЧЕНИЯ
		for i in ($Board as Control).get_children():
			if not i is TextureRect:
				# задержка через одну колонку
				match j:
					0.0, 4.0:
						i.stop_icons(0.0)
					1.0, 3.0:
						i.stop_icons(0.15)
					2.0:
						i.stop_icons(0.3)

				# задержка слева направо
				# i.stop_icons(j / 7.0)

				j += 1.0
		yield(get_tree().create_timer(1.5), "timeout")
		get_node("/root/Main/GUI").call_deferred("set_score")
		_spin_stop()


# вызов начала автоспина
func auto_spin() -> void:
	if not spinning and not auto_spinning:
		set_auto_spinning(true)
		spin()


# остановка спина. повторяется
func _spin_stop() -> void:
	set_spinning(false)
	if auto_spinning:
		yield(get_tree().create_timer(1.5), "timeout")
		_update_autospin_count()


# сигнал таймера паузы. перезапускает спин автоматически
func _update_autospin_count() -> void:
	# если меньше максимального количества спинов, то продолжаем крутить
	if Global.get("AUTO_SPIN_MAX") > Global.get("AUTO_SPIN_COUNT"):
		Global.call("set_autospin_count", Global.get("AUTO_SPIN_COUNT") + 1)
		spin()
	# если закончили, то останавливаем автоспины
	else:
		set_spinning(false)
		set_auto_spinning(false)
		Global.call("set_autospin_count", 1)
	get_node("/root/Main/GUI").call_deferred("change_autospin_count")


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	prepare_level(payload.level)


# SIGNALS -------------------------


