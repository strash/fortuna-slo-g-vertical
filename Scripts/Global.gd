extends Node


# енум имен сцен
enum VIEW_MAP {
	START, # стартовый экран
	SLOT_GAME, # экран игры слотово
	ROULETTE_GAME, # экран игры рулетки
	INFO, # экран с текстовой информацией
	LEVELS, # экран с выбором уровней (слотов/рулетки)
	LEVEL_MENU, # экран меню уровня
}
# имена всех сцен
const VIEWS: PoolStringArray = PoolStringArray([
	"res://Scenes/Start.tscn",
	"res://Scenes/SlotGame.tscn",
	"res://Scenes/RouletteGame.tscn",
	"res://Scenes/Info.tscn",
	"res://Scenes/Levels.tscn",
	"res://Scenes/LevelMenu.tscn",
])

# GUI
const GIFT_PRICE: int = 60000 # gift price


# GAME
const SPIN_TIME: float = 2.3 # время кручения после разгона
var AUTO_SPIN_COUNT: int = 1 # счетчик количества повторов автоспина
const AUTO_SPIN_MAX: float = 10.0 # количество повторов автоспина

const BGS: Array = [
	preload("res://Resources/Sprites/Slot/bg_lvl_1.png"),
	preload("res://Resources/Sprites/Slot/bg_lvl_2.png"),
	preload("res://Resources/Sprites/Slot/bg_lvl_3.png"),
]
const BOARDS: Array = [
	preload("res://Resources/Sprites/Slot/board_lvl_1.png"),
	preload("res://Resources/Sprites/Slot/board_lvl_2.png"),
	preload("res://Resources/Sprites/Slot/board_lvl_3.png"),
]
const SHADOWS: Array = [
	#[
	#	null, null,
	#],
]

const PROPS: = [
	{
		w = 226,
		h = 201,
		count_x = 3, # количество по горизонтали
		count_y = 3, # количество по вертикали
		offset_x = -5, # отступ между иконками
		offset_y = 30,
		variations = 4, # вариации иконок
		icons_offset_x = 0, # отступ иконок
		icons_offset_y = -10,
		board_bg_offset_x = 0, # отступ картинки доски
		board_bg_offset_y = 0,
		shadow_top_offset_y = 0, # отступ верхней тени
		shadow_bottom_offset_y = 0, # отступ нижней тени
	},
	{
		w = 202,
		h = 182,
		count_x = 3,
		count_y = 3,
		offset_x = 25,
		offset_y = 65,
		variations = 4,
		icons_offset_x = 0,
		icons_offset_y = 0,
		board_bg_offset_x = 0,
		board_bg_offset_y = 0,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
	{
		w = 195,
		h = 193,
		count_x = 3,
		count_y = 3,
		offset_x = 34,
		offset_y = 48,
		variations = 4,
		icons_offset_x = 0,
		icons_offset_y = -4,
		board_bg_offset_x = 0,
		board_bg_offset_y = 0,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
]


# METHODS -------------------------


func set_autospin_count(count: int) -> void:
	AUTO_SPIN_COUNT = count


