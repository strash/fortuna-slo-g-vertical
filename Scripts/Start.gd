extends Control


signal change_view_requested


# описание видимости блоков GUI
const GUI_SETUP: Dictionary = {
	"is_header_block_visible": false,
	"is_footer_block_visible": false,
	"is_score_block_visible": false,
	"is_level_block_visible": false,
	"is_btn_back_block_visible": false,
}


# BUILTINS -------------------------


func _ready() -> void:
	var particles: Particles2D = $Logotype/Particles2D
	var logo: TextureRect = $Logotype
	particles.emitting = true
	particles.position = logo.rect_size / 2.0


# METHODS -------------------------


# SETGET -------------------------


# SIGNALS -------------------------


func _on_BtnPlay_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT, { "level": 1 })


